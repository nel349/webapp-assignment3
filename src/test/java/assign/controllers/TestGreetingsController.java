package assign.controllers;

import java.awt.List;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;





import javax.servlet.http.HttpServletRequest;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.junit.Assert.assertEquals;
import assign.controllers.GreetingsController;
import assign.services.GreetingsServices;

import org.junit.Before;
import org.junit.Test;
import org.springframework.web.servlet.ModelAndView;

public class TestGreetingsController {
	
	public GreetingsController gc;
	public GreetingsServices gs;
	
	private static GreetingsController mockedGC;
	private static GreetingsServices mockedGS;

	@Before
	public void setUp() {
		gc = new GreetingsController();
		gs = new GreetingsServices();
		
		mockedGC  = mock(GreetingsController.class);
		mockedGS  = mock(GreetingsServices.class);
		String[] url1 = {"meetings", "barbican","2013"};

		
	    when(mockedGS.getURL(url1)).thenReturn("http://eavesdrop.openstack.org/meetings/barbican/2013");
//	    when(mockedGC.getBook("8131721019")).thenReturn(book1);
//	    when(mockedGC.addBook(book1)).thenReturn(book1.getIsbn());
//
//	    when(mockedGC.updateBook(book1)).thenReturn(book1.getIsbn());
	    
		HttpServletRequest request = mock(HttpServletRequest.class);       
	    when(mockedGC.gettype(url1, request)).thenReturn(new ModelAndView());
	
	
	}
	
	@Test
	public void test_getUrl_1() {
		String[] url1 = {"meetings", "barbican","2013"};
		assertEquals("http://eavesdrop.openstack.org/meetings/barbican/2013", gs.getURL(url1));
	}
	@Test
	public void test_getUrl_2() {
		String[] url2 = {"irclogs", "heat",""};
		assertEquals("http://eavesdrop.openstack.org/irclogs/%23heat/", gs.getURL(url2));
	}
	
	
	
	@Test
	public void test_getQueryData_1() {
		String[] url1 = {"meetings", "barbican","2013"};
		String url =gs.getURL(url1);
		Document doc =null;
		try {
			doc = Jsoup.connect(url).get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
			 
		String all = gs.getQueryData(doc);
		String expected = "<br><a href=http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.html>http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.html</a><br><a href=http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.log.html>http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.log.html</a><br><a href=http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.log.txt>http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.log.txt</a><br><a href=http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.txt>http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.txt</a>";
				
		
		assertEquals(expected,all);
	}
	@Test
	public void test_getQueryData_2() throws IOException {
		String[] url1 = {"meetings", "barbican","2013"};
		String url =mockedGS.getURL(url1);
		Document doc = Jsoup.connect(url).get();
		String all = gs.getQueryData(doc);
		String expected = "<br><a href=http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.html>http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.html</a><br><a href=http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.log.html>http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.log.html</a><br><a href=http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.log.txt>http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.log.txt</a><br><a href=http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.txt>http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.txt</a>";
				
		
		assertEquals(expected,all);
	}
	
	
	@Test
	public void test_getData_1() {
		String[] url1 = {"meetings", "barbican","2013"};
		ArrayList<String> list  = new ArrayList<String>();
		
		String actual = this.gs.getData(url1, new ModelAndView(), list);
		String expected = "<br><a href=http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.html>http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.html</a><br><a href=http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.log.html>http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.log.html</a><br><a href=http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.log.txt>http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.log.txt</a><br><a href=http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.txt>http://eavesdrop.openstack.org/meetings/barbican/2013/barbican.2013-12-12-19.58.txt</a>";
		assertEquals(expected, actual);
		

	}
	
	@Test
	public void test_getType_1() {
		String[] url1 = {"meetings", "barbican","2013"};
		HttpServletRequest request = mock(HttpServletRequest.class);       

		ModelAndView v = this.mockedGC.gettype(url1, request);
		validateMockitoUsage();
	}
	
	@Test
	public void test_getType_2() {
		String[] url1 = {"meetings", "barbican","2013"};
		HttpServletRequest request = mock(HttpServletRequest.class);       

		
		
	}
	
	

}
