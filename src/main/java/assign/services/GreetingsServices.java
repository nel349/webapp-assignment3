package assign.services;

import java.io.IOException;
import java.util.List;


import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.web.servlet.ModelAndView;

public class GreetingsServices {

	public String getURL(String[] link){
		String type = link[0];
		String project = link[1];
		String year = link[2];
		String contextPath= "http://eavesdrop.openstack.org/";
		if(type.equals("irclogs") && !project.isEmpty())
			project = "%23" + project;
		String wholePath = contextPath + type + "/"+ project +"/" + year;
		return wholePath;
	}
	
	public String getData(String[] link, ModelAndView model , List<String> list){
		

		String url =this.getURL(link);
		list.add(url);
		Document doc =null;
		try {
			doc = Jsoup.connect(url).get();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		model.setViewName("displayPage");
		if(doc == null){
			return "SORRY, something went wrong, try again. invalid input or query could not be executed";
		}
			
		return this.getQueryData(doc);
	}
	
	public String getQueryData(Document doc){
		Elements links = doc.select("a[href]");
		String all ="";
		for (Element s_links : links) {
			String x = s_links.absUrl("href");
			if(x.endsWith(".txt") || x.endsWith(".html") || x.endsWith(".log"))
				all += "<br>" + "<a href="+ x +">" + x +"</a>" ;
		}
		return all;
	}
	
}
