package assign.controllers;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import assign.services.GreetingsServices;


@Controller
public class GreetingsController {
	Logger infoLogger = Logger.getLogger("assignment3");

	ArrayList<String> link_list = new ArrayList<String>();
	List<String> list = this.link_list;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView singleFieldPage(HttpServletRequest request)  {
		this.infoLogger.info("Enter displayPage.jsp");
		ModelAndView mav = new ModelAndView("displayPage");
		HttpSession se = request.getSession(false);
		try{
			if(se.getAttribute("in_session") == null){
				mav.addObject("sid", "NO SESSION");
				link_list.clear();
				this.infoLogger.info("THERE IS NO SESSION");

			}else{
				mav.addObject("lists", list);
				mav.addObject("sid", "IN SESSION");
			}
		}
		catch(java.lang.NullPointerException e){
			e.printStackTrace();
		}
		return mav; 
	}

	@ResponseBody
	@RequestMapping(value = "/submitquery", params = {"submitquery"}, method={RequestMethod.POST})
	public ModelAndView gettype(@RequestParam("link") String[] link, HttpServletRequest request)
	{
		GreetingsServices service = new GreetingsServices();

		ModelAndView model = new ModelAndView();

		String all =service.getData(link, model, list);

		model.addObject("links", all);
		HttpSession s = request.getSession(true);
		s.setAttribute("in_session", "IN SESSION");
		model.addObject("sid", s.getAttribute("in_session"));
		model.addObject("lists", list);

		return model;
	}



	@RequestMapping(method = RequestMethod.POST, value="/logout")
	public ModelAndView processContactForm(HttpServletRequest req) {      
		req.getSession(false).invalidate();
		ModelAndView model = new ModelAndView();
		model.addObject("sid", "NO SESSION");
		model.setViewName("displayPage");
		link_list.clear();
		return model;
	}



}
